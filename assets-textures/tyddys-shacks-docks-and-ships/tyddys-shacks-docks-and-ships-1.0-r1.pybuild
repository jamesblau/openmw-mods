# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import InstallDir, Pybuild1
from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "Shacks Docks and Ships - Arkitektora of Vvardenfell"
    DESC = "Contains new textures for wooden shacks, docks and ships"
    HOMEPAGE = """
        https://www.nexusmods.com/morrowind/mods/43520
        https://www.nexusmods.com/morrowind/mods/45336
    """
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    TEXTURE_SIZES = "1024 2048"
    NEXUS_SRC_URI = """
        texture_size_1024? (
            https://www.nexusmods.com/morrowind/mods/43520?tab=files&file_id=1000003470
            -> MQ_Textures-43520-1-0.7z
        )
        texture_size_2048? (
            https://www.nexusmods.com/morrowind/mods/43520?tab=files&file_id=1000003469
            -> HQ_Textures-43520-1-0.7z
        )
        map_normal? (
            https://www.nexusmods.com/morrowind/mods/45336?tab=files&file_id=1000021691
            -> 01_Shacks_Docks_and_Ships-45336-1-1-1605034914.7z
        )
    """
    # modding-openmw.com recommends removing these files to avoid overriding other
    # mods that provide wood textures
    BLACKLIST = [
        "Textures/Tx_wood_brown_shelf_corner.dds",
        "Textures/Tx_wood_brown_shelf_corner_01.dds",
        "Textures/Tx_wood_brown_shelf.dds",
    ]
    IUSE = "map_normal"
    INSTALL_DIRS = [
        InstallDir(
            "MQ/Data Files",
            S="MQ_Textures-43520-1-0",
            BLACKLIST=BLACKLIST,
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            "HQ/Data Files",
            S="HQ_Textures-43520-1-0",
            BLACKLIST=BLACKLIST,
            REQUIRED_USE="texture_size_2048",
        ),
        # textures for Windows glow
        # InstallDir("MQ/Extras/Windows Glow", S="MQ_Textures-43520-1-0"),
        # InstallDir("HQ/Extras/Windows Glow", S="HQ_Textures-43520-1-0"),
        InstallDir(
            "01 Shacks docks and ships",
            S="01_Shacks_Docks_and_Ships-45336-1-1-1605034914",
            REQUIRED_USE="map_normal",
        ),
    ]
