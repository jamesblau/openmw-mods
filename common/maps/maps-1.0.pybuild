# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import Pybuild1


class Package(Pybuild1):
    NAME = "OptMaps"
    DESC = "Sets openmw settings for packages which optionally support texture maps"
    KEYWORDS = "openmw tes3mp"


def dict_create_or_set(dictionary, key, value):
    if key in dictionary:
        if isinstance(value, dict) and isinstance(dictionary[key], dict):
            for k, v in value.items():
                dict_create_or_set(dictionary[key], k, v)
        else:
            dictionary[key] = value
    else:
        dictionary[key] = value


class OptMaps:
    """
    Class which ensures that the openmw settings
    to make use of various map types are enabled
    via the map use expand variables
    """

    def src_prepare(self):
        super().src_prepare()
        map_dicts = {
            "map_normal": {"auto use object normal maps": "True"},
            "map_specular": {"auto use object specular maps": "True"},
            "map_terrain_normal": {"auto use terrain normal maps": "True"},
            "map_terrain_specular": {"auto use terrain specular maps": "True"},
        }
        for key in map_dicts:
            if key in self.USE:
                if hasattr(self, "SETTINGS") and self.SETTINGS is not None:
                    dict_create_or_set(self.SETTINGS, "Shaders", map_dicts[key])
                else:
                    self.SETTINGS = {"Shaders": map_dicts[key]}
