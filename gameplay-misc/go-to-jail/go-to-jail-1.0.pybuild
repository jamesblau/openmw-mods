# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import Pybuild1, InstallDir, File
from pybuild.info import P
from common.util import CleanPlugin


class Package(CleanPlugin, Pybuild1):
    NAME = "Morrowind Go To Jail"
    DESC = "Makes going to jail more like Oblivion"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/37792"
    LICENSE = "attribution-distribution"  # See nexus mods permissions info
    RDEPEND = "base/morrowind[bloodmoon?,tribunal?]"
    DEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    SRC_URI = f"""
        bloodmoon? (
            tribunal? ( {P}-mournhold-solstheim.zip )
            !tribunal? ( {P}-solstheim.zip )
        )
        !bloodmoon? ( tribunal? ( {P}-mournhold.zip ) )
        !tribunal? ( !bloodmoon? ( {P}.zip ) )
    """
    IUSE = "bloodmoon tribunal"

    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[File("Go To Jail (Mournhold + Solshteim).ESP", CLEAN=True)],
            S=f"{P}-mournhold-solstheim",
            REQUIRED_USE="bloodmoon tribunal",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("Go To Jail (Solstheim).ESP", CLEAN=True)],
            S=f"{P}-solstheim",
            REQUIRED_USE="bloodmoon !tribunal",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("Go To Jail (Mournhold).ESP", CLEAN=True)],
            S=f"{P}-mournhold",
            REQUIRED_USE="tribunal !bloodmoon",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("Go To Jail.esp", CLEAN=True)],
            S=f"{P}",
            REQUIRED_USE="!tribunal !bloodmoon",
        ),
    ]
