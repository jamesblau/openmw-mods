# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from pybuild import Pybuild1, InstallDir, File
from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "Skies"
    DESC = "Comprehensive Overhaul for the weather in Morrowind, Solsthiem, and beyond"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43311"
    LICENSE = "all-rights-reserved"
    KEYWORDS = "openmw"
    RES_FILE = "Skies_.IV_Resource_Pack-43311--IV"
    MAIN_FILE = "Skies_.IV-43311--IV"
    SRC_URI = f"""
        minimal? ( {RES_FILE}.zip )
        !minimal? ( {MAIN_FILE}.zip )
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/43311"
    TEXTURE_SIZES = "4096"
    IUSE = """
        +night-sky-mesh +sky-mesh +ash-cloud-mesh +rain-particle-mesh
        minimal
        concept-art-masser
        moons
        moons-alt
        +particles
        extra-clear-sky
        vanilla
        extreme-textures
        aof-skies
        devtools
    """
    REQUIRED_USE = """
        !minimal? (
            night-sky-mesh
            sky-mesh
            particles? (
                ash-cloud-mesh
                rain-particle-mesh
            )
        )
        minimal? (
            !concept-art-masser
            !moons
            !moons-alt
            !particles
            !extra-clear-sky
            !vanilla
            !extreme-textures
            !aof-skies
        )
    """
    INSTALL_DIRS = [
        InstallDir(
            ".",
            S=RES_FILE,
            REQUIRED_USE="minimal",
            FILES=[
                File("Sky Mesh Texture Guide.png", REQUIRED_USE="devtools"),
                File("Sky Mesh Texture Guide.psd", REQUIRED_USE="devtools"),
                File("Readme - For modders ONLY!!.txt", REQUIRED_USE="devtools"),
                File("meshes/ashcloud.nif", REQUIRED_USE="ash-cloud-mesh"),
                File("meshes/raindrop.nif", REQUIRED_USE="rain-particle-mesh"),
                File("meshes/sky_clouds_01.nif", REQUIRED_USE="sky-mesh"),
                File("meshes/sky_night_02.nif", REQUIRED_USE="night-sky-mesh"),
            ],
        ),
        InstallDir(
            "Skies - Extra Clear sky texture",
            S="Skies_.IV-43311--IV",
            RENAME="textures",
            REQUIRED_USE="!minimal extra-clear-sky",
        ),
        InstallDir(
            "Skies - Vanilla",
            S="Skies_.IV-43311--IV",
            RENAME="textures",
            REQUIRED_USE="!minimal vanilla",
        ),
        InstallDir(
            "Skies - AoF Skies",
            S="Skies_.IV-43311--IV",
            REQUIRED_USE="!minimal aof-skies",
        ),
        InstallDir(
            "Moons (alt)",
            S="Skies_.IV-43311--IV",
            RENAME="textures",
            REQUIRED_USE="!minimal moons-alt",
        ),
        InstallDir(
            "Moons",
            S="Skies_.IV-43311--IV",
            RENAME="textures",
            REQUIRED_USE="!minimal moons",
        ),
        InstallDir(
            "Moon Alt - Concept Art Inspired Masser",
            S="Skies_.IV-43311--IV",
            RENAME="textures",
            REQUIRED_USE="!minimal concept-art-masser",
        ),
        InstallDir(
            "Particles", S="Skies_.IV-43311--IV", REQUIRED_USE="!minimal particles"
        ),
        InstallDir(
            "Skies - Extreme Textures",
            S="Skies_.IV-43311--IV",
            REQUIRED_USE="!minimal extreme-textures",
        ),
        InstallDir("Skies - .IV", S="Skies_.IV-43311--IV", REQUIRED_USE="!minimal"),
    ]
    FALLBACK = {
        "Weather Clear": {"Cloud Speed": "0.25"},
        "Weather Cloudy": {"Cloud Speed": "0.4"},
        "Weather Foggy": {"Cloud Speed": "0.25"},
        "Weather Thunderstorm": {"Cloud Speed": "0.6"},
        "Weather Rain": {"Cloud Speed": "0.4"},
        "Weather Overcast": {"Cloud Speed": "0.3"},
        "Weather Ashstorm": {"Cloud Speed": "1.4"},
        "Weather Blight": {"Cloud Speed": "1.8"},
        "Weather Snow": {"Snow Cloud Speed": "0.3"},
        "Weather Blizzard": {"Cloud Speed": "1.5"},
    }

    def src_prepare(self):
        # Causes rain to be invisible
        # See https://gitlab.com/portmod/openmw-mods/-/issues/185
        os.remove("Particles/textures/tx_raindrop_01.dds")
